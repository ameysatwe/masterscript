#!/bin/bash

git --version # will print git version

SPTH='/usr/local/apache2/htdocs/' #set SPTH as a variable


pwd # check where the script is starts
cd .. || exit # cd out of the folder
echo "its working till here" # debug echo
pwd # check the path
ls -ltr # list the directory contents

if [ -d "round-template" ]; then rm -rf "round-template"; fi  #check if a directory called round-template exists if it does remove the that dir
if [ ! -d "round-template" ]; then mkdir "round-template"; fi # check if a directory called round-template does not exist. if it Doesn’t  make that dir.
pwd # check the path
ls -ltr # list the directory contents
cp -r pedagogy storyboard lab-manual README.md /usr/local/apache2/round-template/ # copy all the experiment folders into round-template directory
cp -r /usr/local/apache2/build/simulation /usr/local/apache2/round-template/ # copy the built files into round-template. Files are built in the first docker.
mydir="$(pwd)" #set mydir to pwd
echo "$mydir" # check mydir

if [ ! -d "vlabsdev-template-master" ]; # check if a directory called vlabsdev-template-master does'nt exist  
then
#wget https://gitlab.com/ameysatwe/newtemplate/-/archive/master/newtemplate-master.zip
wget "https://gitlab.com/api/v4/projects/24158608/repository/archive.zip?private_token=3qBuEs-_Bs_p6_T9Xjvq"
unzip archive.zip*
mv vlabsdev-template-master* vlabsdev-template-master
#unzip newtemplate-master.zip
#unzip vlabsdev-template-master.zip
# if it doesnt download the template from the gitlab repo
else
cp -r vlabsdev-template-master/ / # Else if there is a directory then copy contents to root
fi
ls -ltr # debug ls

cd "$mydir" || exit # cd into mydir or exit
cp -r /usr/local/apache2/round-template/ /usr/local/apache2/vlabsdev-template-master/ # copy round-template into newtemplate-master
cd /usr/local/apache2/vlabsdev-template-master/ || exit # cd into new-template

if [ ! -d "images" ]; then mkdir "images"; fi # If a directory images does not exist then make a directory images 
cd round-template/lab-manual/ || exit # cd into lab-manual directory
cp -r images/* ../../images/  # copy all the images from lab-manual into top level images directory
pwd # print the working directory
cd ../simulation || exit # cd into simualation
ls -ltr # debug ls to check contents of ls
if [ ! -d "jS" ]; then mkdir "jS"; fi # check if a directory for jS does not exist then make a directory for jS 
cp -r /usr/local/apache2/build/simulation/jS /jS #copy the built files from build/simulation/jS to newtemplate-master/round-template/simulation/jS
cd jS/ # cd into jS
echo "Inside jS" # debug echo
ls -ltr  # debug ls to check the built files
cd .. # cd out of jS/
echo "I am here outside jS" # debug echo
cp -r css/ /css # copy css to its folder /css => root path
cp -r images /images # copy images to the /images => root path
cp -r *.html / # copy (all).html files to root
cd ../../../ || exit # cd out of all the sub folders

echo "its working till here also" # debug echo

# basename=$(basename "$(git remote get-url origin)" .git)
#echo "$basename" This was used earlier to set folder name as basename for repo
cd vlabsdev-template-master || exit
ls -ltr # ls to get contents inside newtemplate
cp -r . "$SPTH" # copy contents of newtemplate to $SPTH => /usr/local/apache2/htdocs/ 
echo "$SPTH" # debug echo to check $SPTH 
echo "Copy Done" # debug echo to indicate copy done
ls "$SPTH" # debug list to check what files were copied inside $SPTH
